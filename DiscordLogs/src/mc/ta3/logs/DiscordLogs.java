package mc.ta3.logs;



import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.event.Listener;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;


public class DiscordLogs extends JavaPlugin implements Listener{

	public void onEnable(){
		
		if(getServer().getPluginManager().getPlugin("DiscordMC") == null){
			Bukkit.getConsoleSender().sendMessage(ChatColor.GREEN+"[DiscordLogs] "+ChatColor.RED+"Error: DiscordMC not found!");
			Bukkit.getPluginManager().disablePlugin(this);
			return;
		}
		PluginManager pm = Bukkit.getServer().getPluginManager();
		pm.registerEvents(new DiscordEvent(), this);
		  
		  

		 
		 Bukkit.getConsoleSender().sendMessage(ChatColor.GREEN+"[DiscordLogs] Completed!");
		 
		 return;
	}
	
	public void onDisable(){
		 Bukkit.getConsoleSender().sendMessage(ChatColor.GREEN+"[DiscordLogs] Disabled!");
	}
	
	
	
}
