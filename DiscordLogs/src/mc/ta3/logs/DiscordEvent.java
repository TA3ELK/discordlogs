package mc.ta3.logs;


import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;

import eu.manuelgu.discordmc.MessageAPI;



public class DiscordEvent implements Listener {
	


	@EventHandler
	public void onCommand(PlayerCommandPreprocessEvent event) {

		Player player = event.getPlayer();
		String mes = event.getMessage();
		
		if(!player.hasPermission("discordlogs.hide")){
		 MessageAPI.sendToDiscord("[CMD|"+player.getName()+"]: "+mes);	
		}
		 
	}
	
}
